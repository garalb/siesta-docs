.. sectionauthor:: Yann Pouillon <y.pouillon@simuneatomistics.com>

Preparing the environment
=========================

The following actions are essential for the build of SIESTA to succeed. They
have to be performed at least once. Then, depending on the installation method
used, updating the available toolchains and libraries will be either automatic
or manual.


On Linux
--------

Debian-based distributions with GCC
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On distributions like Debian, Mint or Ubuntu, the minimum requirements can be
installed system-wide with the *apt* package manager. To enable the default
GCC-based toolchain, it is as simple as typing::

   sudo apt install build-essential gfortran

The *build-essential* package makes sure that the C and C++ compilers from
GCC, as well as GNU Make, are always installed.

If you want to benefit from enhanced performance, you can also install a MPI
distribution. On Debian and its derivatives, the most widely used
implementation is OpenMPI::

   sudo apt install libopenmpi-dev openmpi-bin

We will assume in the following that you have installed OpenMPI. If this is
not the case, simply remove the `-mpi-` packages from the install arguments of
*apt*.

Although SIESTA 4.x comes with an embedded linear algebra implementation, it
is highly recommended to use a native one, e.g. a recent version from Netlib.
With *apt*, this reads::

   sudo apt install libblas-dev liblapack-dev libscalapack-mpi-dev

For platform-independent I/O with HDF5 and NetCDF, the following packages are
necessary::

   sudo apt install libhdf5-dev libhdf5-mpi-dev libnetcdf-dev libnetcdff-dev netcdf-bin

Please note that HDF5 is a very complex set of libraries, in particular when it
comes to configuring and using the correct ones. You can expect a few teething
problems if you are not yet familiar with it.

Finally, if you intend to use the FLOOK interface, the readline package is
also needed::

   sudo apt install readline-common

If you intend to download the source code of SIESTA from internet, we recommend
you to install *wget* or *curl*. Both work very well, choosing one over the
other is mostly a question of personal preference. If you are not sure, you can
install both::

   sudo apt install curl wget

In some cases, you may have to apply patches to some source file before compiling SIESTA.
For this, you need difference-related utilities::

   sudo apt install diffutils patch

Although this is not something you will use daily, these tools are still quite
handy.


RPM-based distributions with GCC
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On distributions like RedHat, CentOS or Fedora, the minimum requirements can be installed system-wide with the *yum* package manager.

On CentOS, the first step is to install the *epel-release* repository::

   sudo yum install epel-release

It will be useful to get up-to-date libraries. After that, you can install the *Development Tools* set of packages and *gfortran*::

   sudo yum groupinstall "Development Tools"
   sudo yum install gcc-gfortran

In order to benefit from a MPI distribution like OpenMPI, you have to install
the corresponding packages::

   sudo yum install openmpi-devel

Linear algebra libraries can then be installed with::

   sudo yum install blas-devel lapack-devel blacs-openmpi-devel scalapack-openmpi-devel

For platform-independent I/O with HDF5 and NetCDF, you have to install the
following packages::

   sudo yum install hdf5-devel hdf5-openmpi-devel netcdf-devel netcdf-fortran-devel

Please note that HDF5 is a very complex set of libraries, in particular when it
comes to configuring and using the correct ones. You can expect a few teething
problems if you are not yet familiar with it.

If you intend to download the source code of SIESTA from internet, we recommend
you to install *wget* or *curl*. Both work very well, choosing one over the
other is mostly a question of personal preference. If you are not sure, you can
install both::

   sudo yum install curl wget

The RedHat Development Tools come with utilities to patch source files when
necessary. There is thus no need to install any additional package.


Common issues
~~~~~~~~~~~~~

What happens if you cannot become root on the computer where you would
like to use SIESTA? When possible, you can point your system
administrator to these instructions. Otherwise, if you have enough
disk space available, EasyBuild, Spack, and similar systems provide a way to
install the software in your user space, meaning you don’t need root
access.  This can be particularly useful on shared systems where you
don’t have administrative privileges.


On macOS
--------

It is certainly possible to develop scientific software on a macOS-based computer. In fact, many
developers can be seen at hackathons and elsewhere with their MacBooks.

It is best to take advantage of the Unix-based core of macOS, work in the Terminal, and use a package manager such as
`Homebrew`_ to install the appropriate software stack::

  (Install Homebrew)
  brew install gcc
  brew install veclibfort    # Recommended to complement the built-in optimized LAPACK code
  brew install openblas      # An alternative optimized implementation of LAPACK
  brew install open-mpi
  brew install scalapack
  brew install netcdf-fortran

Then, Siesta can be compiled using, for example::

  cmake -S. -B_build -C Config/cmake/toolchains/mac.cmake  {other options}

The `mac.cmake` toolchain file might need to be edited (e.g. to replace the `Debug` build type
by `Release`, to define the path for locating Scalapack, etc).
  


.. _Homebrew: https://docs.brew.sh/Installation
