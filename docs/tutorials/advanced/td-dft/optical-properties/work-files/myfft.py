import numpy as np
#import scipy
#import matplotlib.pyplot as plt
import sys
from scipy.fftpack import fft 
#import argparse

msg1 = 'Usage: python fft.py [input-file] [output-file]'
msg2 = 'If optional [input-file] or [output-file] are empty,'
msg3 = 'fft.py uses "input.dat" and "output.dat"'
if len(sys.argv) == 1:
	print('======================================================')
	print(msg1)
	print(msg2)
	print(msg3)
	print('======================================================')
#parser = argparse.ArgumentParser(description = msg)
#args = parser.parse_args()

inputfile='input.dat'
outputfile='output.dat'
if len(sys.argv)==2 :
        inputfile = sys.argv[1]
elif len(sys.argv)==3 :
        inputfile = sys.argv[1]
        outputfile = sys.argv[2]

#inputfile = sys.argv[1]
#outputfile = sys.argv[2]

data=np.genfromtxt(str(inputfile),names=['t','px','py','pz'])
#data=np.genfromtxt('input.dat',names=['t','px','py','pz'])
N=len(data['t'])
m=int(N/2)    # the first half of the FT stores positive frequencies. 

#total time
T=data['t'][N-1]

#Sample timestep
dT=data['t'][1] * 1e-15

# 1 Hertz = 4.135665538e-15 eV
scale = 4.135665538e-15

#np.savetxt('output.dat',np.abs(fft(data['px']))/len(data['px']))

#F=data['px']*data['px']+data['py']*data['py']+data['pz']*data['pz']
Fx=data['px']
Fy=data['py']
Fz=data['pz']

AFx=2*fft(Fx)[0:m]
AFy=2*fft(Fy)[0:m]
AFz=2*fft(Fz)[0:m]
#AF=2*fft(data['px'])[0:m]
xF=np.linspace(0.0, scale*1.0/dT,m)

#Save the results to file

np.savetxt(str(outputfile), np.c_[xF,xF*abs(AFx)/N,xF*abs(AFy)/N,xF*abs(AFz)/N])
#np.savetxt('output.dat', np.c_[xF,xF*abs(AF)/N])


