#
# Three quintuple layers of Bi2Se3 
#
# scf convergence + computation of bands
#
# (thanks to Roberto Robles)
#
SystemName   bi2se3_3ql
SystemLabel  bi2se3_3ql

# Geom
NumberOfAtoms       15
NumberOfSpecies      2
%block ChemicalSpeciesLabel
   1  83 Bi
   2  34 Se
%endblock ChemicalSpeciesLabel
LatticeConstant   4.13800000 Ang
%block LatticeVectors
     1.0000000000000000    0.0000000000000000    0.0000000000000000
    -0.5000000000000000    0.8660254037844386    0.0000000000000000
     0.0000000000000000    0.0000000000000000   11.7544706113098112
%endblock LatticeVectors
AtomicCoordinatesFormat Fractional
%block AtomicCoordinatesAndAtomicSpecies
  0.6666666666666667  0.3333333333333333  0.2650624783220366   1   1  Bi
  0.3333333333333333  0.6666666666666667  0.3423936279930260   1   2  Bi
  0.3333333333333333  0.6666666666666667  0.4613344252566094   1   3  Bi
  0.0000000000000000  0.0000000000000000  0.5386655576677342   1   4  Bi
  0.0000000000000000  0.0000000000000000  0.6576063590530268   1   5  Bi
  0.6666666666666667  0.3333333333333333  0.7349374737996754   1   6  Bi
  0.3333333333333333  0.6666666666666667  0.2287521714264216   2   7  Se
  0.0000000000000000  0.0000000000000000  0.3037280662035826   2   8  Se
  0.6666666666666667  0.3333333333333333  0.3787039395991627   2   9  Se
  0.0000000000000000  0.0000000000000000  0.4250241177721818   2  10  Se
  0.6666666666666667  0.3333333333333333  0.5000000000000000   2  11  Se
  0.3333333333333333  0.6666666666666667  0.5749758645633420   2  12  Se
  0.6666666666666667  0.3333333333333333  0.6212960521574118   2  13  Se
  0.3333333333333333  0.6666666666666667  0.6962719343852299   2  14  Se
  0.0000000000000000  0.0000000000000000  0.7712478166130481   2  15  Se
%endblock AtomicCoordinatesAndAtomicSpecies

write-coor-xmol T      # To have the structure in .xyz format

# Basis 
PAO.EnergyShift  100 meV   ! Extension of AOs (smaller => more extended)
PAO.SplitNorm      0.15
PAO.SplitNormH      0.50
PAO.SoftDefault      T

xc.functional         GGA          # Exchange-correlation functional
xc.authors            PBE          # Exchange-correlation version

%block DM.InitSpin
 1  0.  0. 0.
 2  0.  0. 0.
 3  0.  0. 0.
 4  0.  0. 0.
 5  0.  0. 0.
 6  0.  0. 0.
 7  0.  0. 0.
 8  0.  0. 0.
 9  0.  0. 0.
10  0.  0. 0.
11  0.  0. 0.
12  0.  0. 0.
13  0.  0. 0.
14  0.  0. 0.
15  0.  0. 0.
%endblock DM.InitSpin

SolutionMethod  diagon

%block kgrid_Monkhorst_Pack
   3   0   0   0.0
   0   3   0   0.0
   0   0   1   0.0
%endblock kgrid_Monkhorst_Pack

 ################ SCF options #############
SolutionMethod  diagon
ElectronicTemperature   0.0010 eV
MaxSCFIterations   300
DM.NumberPulay   3
DM.MixingWeight     0.0100
MeshCutoff        300.0000 Ry
DM.Tolerance        1.E-4
SCF.H.Converge  F
UseSaveData     F
DM.UseSaveDM    T
SpinOrbit       T

#
#  The BZ is like that of graphene
#
BandLinesScale     ReciprocalLatticeVectors
%block Bandlines
  1   0.5000000000   0.000000000   0.0000   M
 30   0.0000000000   0.000000000   0.0000   \Gamma
 45   0.3333333333   0.333333333   0.0000   K
 30   0.5000000000   0.500000000   0.0000   M
%endblock BandLines


