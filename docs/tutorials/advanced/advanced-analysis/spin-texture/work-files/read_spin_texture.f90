! This file has been contributed by Roberto Robles,
! and modified by Alberto Garcia to adapt it to the
! (shortcomings) of the current output format of the
! 'spin_texture' program.
!
!************************************************************
! WARNING: In this version, the number of k-points, energies,
! Efermi, and bands to process, are entered through standard
! input.
!************************************************************

! It is included to provide some perspective on ways to
! plot the spin-texture information.
! It might need some re-formatting of the default spin-texture
! data provided by the program 'spin_texture' in this directory.
! An alternative to the procedure indicated is to select a single
! band with 'spin_texture', and do some semi-automatic processing
! of the resulting file for input to gnuplot or similar tools.
!
! One needs to plot a vector field where each point k is assigned a
! vector (Sx, Sy, Sz). To achieve this satisfactorily is not
! completely trivial, and different techniques can be used.
!
!  Program to extract the spin texture from file spin_texture.dat as
!  printed by SIESTA. It requires the number of bands and their index
!  provided interactively.
!
!  If executed as "read_spin_texture_xsf" it gives the output as the
!  head of a xsf file, elsewhere it produces a .xyz file.
!
      program read_spin_texture

      double precision, allocatable :: kpoints(:,:), st(:,:,:), en(:,:)

      integer, allocatable :: bands(:)

      integer :: nkp, nen, nb, band
      double precision:: ef

      logical :: xyz
      logical :: dat

      character (len = 100) :: file, ab, name

      open(unit=9,file='spin_texture.dat',status='old')

      !     Skip one lines
      read(9,*)

      ! Read from standard input
      write(*,*) "Enter number of k-points, bands per k-point, and Efermi"
      read(*,*) nkp, nen, ef

      write(*,"(a,2i6,f10.4)") " --> k-points, bands per k-point, Efermi:", nkp, nen, ef

      allocate( kpoints(3,nkp) )
      allocate( st(3,nkp,nen) )
      allocate( en(nkp,nen) )
      allocate( bands(nen) )

      call get_command_argument(0,name)
      if(name.eq."read_spin_texture_xyz" &
         .OR.name.eq."./read_spin_texture_xyz") then
        dat = .false.
        xyz = .true.
      elseif(name.eq."read_spin_texture_dat" &
         .OR.name.eq."./read_spin_texture_dat") then 
        dat = .true.
        xyz = .false.
      elseif(name.eq."read_spin_texture_xsf" &
         .OR.name.eq."./read_spin_texture_xsf") then 
        dat = .false.
        xyz = .false.
      else
        write(*,*) "Invalid format supported: xsf,xyz,dat"
        error stop 1
      endif

      if (xyz) then
        write(*,*) "Output in .xyz format"
      else if (dat) then
        write(*,*) "Output in .dat format"
      else
        write(*,*) "Output for .xsf format"
      endif

      do ik=1,nkp
        ! Skip one line
        read(9,*)
        read(9,'(14x,3f12.6)') (kpoints(j,ik),j=1,3)
!        write(*,'(i4,3f12.6)') ik,(kpoints(j,ik),j=1,3)
        ! Skip one line
        read(9,*)
          do ie=1,nen
            read(9,'(7x,f12.5,3f8.4)') en(ik,ie),(st(j,ik,ie),j=1,3)
!            write(*,'(i4,f12.5,3f8.4)') ie,en(ik,ie),(st(j,ik,ie),j=1,3)
          enddo
      enddo

      write(*,'(a)') "Number of bands: "
      read(*,'(i4)') nb
      do ib=1,nb
        write(*,'(a,i4)') "Index of band # ", ib
        read(*,'(i4)') bands(ib)
        if (bands(ib) .gt. nen) then
          write(*,*) "Band index can not be bigger than ", nen
          stop
        endif

        write(ab,'(i0)') bands(ib)
        if (xyz) then
          file = "st_band_"//trim(ab)//".xyz"
        else if (dat) then 
          file = "st_band_"//trim(ab)//".dat"
        else
          file = "st_band_"//trim(ab)//".xsf"
        end if

        ifile = 67

        open(ifile,file=file)

        ! Write header
        if (xyz) then
          write(ifile,'(i4/)') nkp
        else if (dat) then
          ! No header
        else
          write(ifile,'(a/a)') "set xsfStructure {","ATOMS"
        end if

        ! Write data
        do ik=1,nkp
          if(xyz) then
            write(ifile,'(a,3f12.6,3f8.4)') "X ",&
               (kpoints(j,ik)*10,j=1,2), (en(ik,bands(ib))-ef), &
               (st(j,ik,bands(ib)),j=1,3)
          else if(dat) then
            write(ifile,'(3f12.6,3f8.4)') &
               (kpoints(j,ik)*10,j=1,2), (en(ik,bands(ib))-ef), &
               (st(j,ik,bands(ib)),j=1,3)
           else
            write(ifile,'(i3,3f12.6,3f8.4)') 0,&
               (kpoints(j,ik)*10,j=1,2), (en(ik,bands(ib))-ef), &
               (st(j,ik,bands(ib)),j=1,3)
           end if
        enddo
        if (.not.xyz .and. .not.dat) write(ifile,'(a)') "}"
        close(ifile)
     enddo


      end program read_spin_texture
