import sisl
from pathlib import Path

elec = sisl.Geometry([[0,5,5], [1.5, 5, 5], [3, 5, 5], [4.5, 5, 5]], atoms="C", lattice=sisl.Lattice([6, 10, 10], nsc=[3, 1, 1]))

elec.write(Path(__file__).parent.parent / "electrode" / "elec_geom.fdf")
