import sisl
from pathlib import Path

elec = sisl.Geometry([[0,5,5], [1.5, 5, 5], [3, 5, 5], [4.5, 5, 5]], atoms="C", lattice=sisl.Lattice([6, 10, 10], nsc=[3, 1, 1]))

contact = elec.tile(2, 0)

additions = sisl.Geometry([[-0.2, 5, 5]], atoms="O", lattice=[1.1, 0, 0])

connected = contact.append(additions, 0).append(contact, 0)

connected.write(Path(__file__).parent.parent / "connected" / "0V" / "device_coords.fdf")
