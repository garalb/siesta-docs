set terminal pngcairo lw 1.5 dashed

# Filenames (Adjust to fit your needs)
# ---------
fname_none='none/example_1.DOS'
fname_pol='polarized/example_1.DOS'
fname_ncol='non-colinear/example_1.DOS'
fname_soc='spin-orbit/example_1.DOS'


# Line Styles
# -----------
set style line 1 lc rgb '#346699' pt 4 ps 1.5 lt 1 lw 2 # blue
set style line 2 lc rgb '#de5a5b' pt 8 ps 1.5 lt 1 lw 2 # red
set style line 3 lc rgb '#000000' pt 6 ps 1.5 lt 1 lw 2 # black
set style line 4 lc rgb '#F0903E' pt 6 ps 1.5 lt 1 lw 2 # orange


# Border
# ------
set style line 101 lc rgb '#000000' lt 1 lw 1
set border 3 front ls 101
set tics nomirror out scale 0.75

# X-Axis
# ------
set xlabel 'E-E_F [eV]'
set xrange [-10:5]

# Y-Axis
# ------
set ylabel 'DOS [1/eV]'
set yrange [0:*]


# Key / Legend
# ------------
set key above


# Plot 1: No spin vs spin polarized
# ---------------------------------
set output 'DOS_none-vs-polarized.png'
plot fname_none u 1:2 w l ls 3 ti 'none', \
     fname_pol u 1:2 w l ls 1 lw 2 ti 'polarized: up', \
     fname_pol u 1:3 w l ls 2 lw 2 ti 'polarized: down',\
     fname_pol u 1:($2+$3) w l ls 4 lw 2 ti 'polarized: total'


# Plot 4: Spin polarized vs non-colinear spins
# --------------------------------------------
set output 'DOS_polarized-vs-non-colinear.png'
plot fname_pol u 1:2 w l ls 1 lw 2 ti 'polarized: up', \
     fname_pol u 1:3 w l ls 2 lw 2 ti 'polarized: down',\
     fname_pol u 1:($2+$3) w l ls 4 lw 2 ti 'polarized: total', \
     fname_ncol u 1:($2+$3) w l ls 3 dt (8,8) ti 'non-colinear'


# Plot 3: Spin polarized vs spin-orbit coupling 
# --------------------------------------------
set output 'DOS_polarized-vs-spin-orbit.png'
plot fname_pol u 1:2 w l ls 1 lw 2 ti 'polarized: up', \
     fname_pol u 1:3 w l ls 2 lw 2 ti 'polarized: down', \
     fname_pol u 1:($2+$3) w l ls 4 lw 2 ti 'polarized: total', \
     fname_soc u 1:($2+$3) w l ls 3 dt (8,8) ti 'spin-orbit'

# ADD your own plots here
