:sequential_nav: next

..  _tutorial-basic-pseudopotentials:

Pseudopotentials
================
..
 Even if we do not worry about creating the pseudopotential, we cannot
 really treat it completely as a black box. In this short tutorial, still to
 be finalized, we will discuss some basic concepts of the use of
 pseudopotentials in Siesta, and particular aspects to take into account.
 * Types of pseudopotential files and where to find them.
   * Traditional PSF pseudopotentials.
   * Newer PSML pseudopotentials. The Pseudo-Dojo database.
 * Core/valence configuration of the pseudopotential and its influence on basis-set generation.
   * Heuristics
   * Handling of semicore states.
 * The Kleinman-Bylander transformation and its options. Ghosts.
 * Does the pseudopotential affect the real-space cutoff needed in Siesta?

Generation of pseudopotentials
******************************
Nowadays, with the availability of databases of curated pseudopotentials, such
as the `Pseudo Dojo <https://www.pseudo-dojo.org>`_, the need to look under the
hood is lessened, but has not disappeared completely!. Thus, we might need to
use the ATOM program to generate our own pseudos.

The ATOM package itself can be installed simply by executing::

  git clone https://gitlab.com/garalb/atom-test
  cd atom-test
  ln -sf arch.make.QuantumMobile arch.make # this step might vary
  make

The documentation and tutorials can be accesed `here <https://docs.siesta-project.org/projects/atom>`_.

