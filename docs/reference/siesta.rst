.. _reference_siesta:

SIESTA user manual
==================

The original version in LaTeX of the Siesta manual, with many
convenience macros, is not easily transformed to rst or to
html. Hence, we are for now providing a `link to the pdf version
<https://siesta-project.org/SIESTA_MATERIAL/Docs/Manuals/siesta-MaX-1.3.0.pdf>`_








